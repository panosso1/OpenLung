# User Interface 

The Velocity Team User interface is based on the mockup here attached.

The user interface Mockup was realized and presented to a Medical Doctor group.

The feedback on the Mockup was positive.

## Mockup 

<img src='control-electronics/Electronic-Modules/User_Interface/OSV-UserInterface-Velocity-Team/UI_Mockup/USER-INTERFACE.png' height="400"></img>


## Mockup Overview

The user interface is focused around the following elements:

- a group of push buttons
- 2 visual alarm light 
- 2 auditory alarm buzzers
- A display

The startup and user steps to start ventilation can be reviewed in the following flowchart diagram:

<img src='control-electronics/Electronic-Modules/User_Interface/OSV-UserInterface-Velocity-Team/UI_Mockup/USER-INTERFACE_flowchart.png' height="900"></img>